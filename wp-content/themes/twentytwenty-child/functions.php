<?php
/* enqueue scripts and style from parent theme */        
function twentytwenty_styles() {
	wp_enqueue_style( 'parent', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'twentytwenty_styles');

add_post_type_support( 'page', 'excerpt' );

function script_enqueuer() {		
	//Use google jquery library instead of WordPress default - footer
	wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', false, '1.11.0', true );
	wp_enqueue_script( 'wow', 'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js', false, '1.1.2', true );
	wp_enqueue_script( 'customize-plugin', get_stylesheet_directory_uri() . '/assets/js/customize-plugin.js', array( 'jquery' ), '1.11.1', true );
	wp_enqueue_style( 'custom-plugin', get_stylesheet_directory_uri() .'/assets/css/custom-plugin.css');
}
add_action( 'wp_enqueue_scripts', 'script_enqueuer' );

// Slider custom post type function
function create_slider_posttype() { 
    register_post_type( 'slider',
        array(
            'labels' => array(
                'name' => __( 'Slider' ),
                'singular_name' => __( 'slider' ),
                'menu_name' => __( 'Slider' ),
                'all_items' => __( 'All slider' )
            ),
            'public' => true,
            'has_archive' => false,
            'with_front' => false,
            'feeds' => true,
            'show_in_rest' => true,
            'publicly_queryable' => true,
 			'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'excerpt' ),
        )
    );
}
add_action( 'init', 'create_slider_posttype' );

// News custom post type function
function create_news_posttype() { 
    register_post_type( 'news',
        array(
            'labels' => array(
                'name' => __( 'News' ),
                'singular_name' => __( 'news' ),
                'menu_name' => __( 'News' ),
                'all_items' => __( 'All news' )
            ),
            'public' => true,
            'has_archive' => false,
            'with_front' => false,
            'feeds' => true,
            'show_in_rest' => true,
            'rewrite' => array('with_front' => false,'slug' => 'news'),
            'publicly_queryable' => true,
 			'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'excerpt' ),
        )
    );
}
add_action( 'init', 'create_news_posttype' );