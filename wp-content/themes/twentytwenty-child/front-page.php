<?php
	/*
		Template Name: Front Page
	*/
	get_header();
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<main id="site-content" role="main">
		<div class="electric-car-section">
			<div class="section-inner">
				<?php $ecs_background_image = get_field('ecs_background_image');
				$ecs_link = get_field('ecs_link'); ?>
				<div class="ecs-wrap"<?php if($ecs_background_image) :?> style="background-image: url(<?php echo $ecs_background_image['url']; ?>)"<?php endif; ?>>
					<?php if($ecs_background_image) :?>
						<img src="<?php echo $ecs_background_image['url']; ?>" alt="" title="">
					<?php endif; ?>
					<div class="ecs-content-wrap">
						<?php if(get_field('ecs_heading')):?>
							<h2 class="ecs-heading"><?php the_field('ecs_heading');?></h2>
						<?php endif;?>
						<?php if(get_field('ecs_content')):?>
							<p class="ecs-content"><?php the_field('ecs_content');?></p>
						<?php endif;?>
						<?php if($ecs_link): ?>
							<a href="<?php echo $ecs_link['url']; ?>" class="btn-secondary"><?php echo $ecs_link['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>

		<div class="industry-career-section">
			<div class="section-inner">
				<div class="ics-wrap">
					<div class="ics-left">
						<?php $industry_query = new WP_Query('page_id=73');
					    while ( $industry_query->have_posts() ) : $industry_query->the_post(); ?>
					    	<div class="ics-item">
					    		<div class="ics-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
					    			<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title();?>" title="<?php the_title();?>">
					    		</div>
					    		<div class="ics-content">
					    			<h2 class="ics-title"><?php the_title();?></h2>
					    			<?php the_excerpt();?>
					    			<a class="btn" href="<?php the_permalink();?>">Learn more</a>
					    		</div>
					    	</div>
					    <?php endwhile;
					    wp_reset_postdata(); ?>	
					</div>
					<div class="ics-right">
						<?php $industry_query = new WP_Query('page_id=69');
					    while ( $industry_query->have_posts() ) : $industry_query->the_post(); ?>
					    	<div class="ics-item">
					    		<div class="ics-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
					    			<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title();?>" title="<?php the_title();?>">
					    		</div>
					    		<div class="ics-content">
					    			<h2 class="ics-title"><?php the_title();?></h2>
					    			<?php the_excerpt();?>
					    			<a class="btn" href="<?php the_permalink();?>">Learn more</a>
					    		</div>
					    	</div>
					    <?php endwhile;
					    wp_reset_postdata(); ?>	
					</div>
				</div>
			</div>
		</div>

		<div class="news-section">
			<div class="section-inner">
				<h2 class="ns-heading">Headlines & Industry News</h2>
				<div class="ns-wrap">
					<div class="nsw-left">
<?php
$args = array(
'post_type'   => 'news',
'post_status' => 'publish',
'posts_per_page' => 1
);
$query = new WP_Query( $args );
	if( $query->have_posts() ) : ?>
	    <?php
	      while( $query->have_posts() ) :
	        $query->the_post();
	        ?>
	        <div class="nsw-item">
	        	<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?></time>
	        	<h3 class="nsw-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        	<div class="nsw-text"><?php the_excerpt();?></div>
	        	<a class="btn" href="<?php the_permalink();?>">Read more</a>
	        </div>
	        <?php
	      endwhile;
	      wp_reset_postdata();
	    ?>
	<?php endif; ?>
					</div>
					<div class="nsw-right">
<?php
$args = array(
'post_type'   => 'news',
'post_status' => 'publish',
'offset' => '1',
'posts_per_page' => 4
);
$query = new WP_Query( $args );
	if( $query->have_posts() ) : ?>
		<div class="nsw-item-wrap">
	    <?php
	      while( $query->have_posts() ) :
	        $query->the_post();
	        ?>
	        <div>
	        <div class="nsw-item">
	        	<time datetime="<?php the_time( 'F n, Y' ); ?>" pubdate><?php the_time( 'F n, Y' ); ?></time>
	        	<h3 class="nsw-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
	        	<a class="nsw-link" href="<?php the_permalink();?>">Read more</a>
	        </div>
	        </div>
	        <?php
	      endwhile;
	      wp_reset_postdata();
	    ?>
		</div>
	<?php endif; ?>						
					</div>
				</div>
			</div>
		</div>

	</main>
<?php endwhile; ?>

<?php // get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer();
