<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
			<footer id="site-footer" role="contentinfo" class="header-footer-group">

				<div class="section-inner">

					<div class="footer-credits">

						<p class="footer-copyright">Copyright 2021. This is a test and not a real project</p><!-- .footer-copyright -->

					</div><!-- .footer-credits -->
					<div class="footer-nav">
			            <?php wp_nav_menu(array(
			              'menu' => 'Footer Menu',
			              'menu_class' => 'menu',
			              )); ?>
			          </div>

				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->
<script type="text/javascript">
	// Init slick slider + animation

</script>
		<?php wp_footer(); ?>

	</body>
</html>
