<?php
/**
 * Displays the home slider and intro content
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

<div class="hero-slider-wrap">
	<div class="section-inner">
		<?php
		$args = array(
		  'post_type'   => 'slider',
		  'post_status' => 'publish',
		 );
		 
		$query = new WP_Query( $args );
		if( $query->have_posts() ) : ?>
		  <div class="hero-slider">
		    <?php
		      while( $query->have_posts() ) :
		        $query->the_post();
		        ?>
		        <div class="slider-items" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
		        	<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title();?>" title="<?php the_title();?>">
		        	<div class="siws-content-wrap animated" data-animation-in="fadeInUp">
		        		<h1><?php the_title();?></h1>
		        	</div>
		        </div>
		        <?php
		      endwhile;
		      wp_reset_postdata();
		    ?>
		  </div>
		<?php endif; ?>
	</div>
</div>
<div class="site-intro-wrap">
	<div class="section-inner">
		<div class="siw-content-wrap">
			<?php if(get_field('his_heading')): ?>
			<div class="siw-left">
				<h2 class="siw-heading"><?php the_field('his_heading');?></h2>
			</div>
			<?php endif; ?>
			<div class="siw-right">
				<?php if(get_field('his_content')): ?><?php the_field('his_content');?><?php endif; ?>			
			</div>
		</div>
	</div>
</div>